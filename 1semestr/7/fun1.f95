function tab(t)
use my_prec
implicit none
real(mp) t, tab
tab=exp(t)*(1.1_mp- sqrt(1.21_mp-exp(-t)))
return
end function