function fune(eps,y,x)
use my_prec
implicit none
real(mp) eps,fune,y,x
fune=eps
y=x+eps
do while (y>x)
fune=fune/10
y=x+fune
enddo
return
end function