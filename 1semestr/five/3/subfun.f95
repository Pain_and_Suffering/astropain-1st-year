subroutine subfun(x,y,res)
use my_prec
implicit none
integer(mp) x,y,res
do while (y.ne.0)
res=mod(x,y)
x=y
y=res
enddo
res=x
end subroutine