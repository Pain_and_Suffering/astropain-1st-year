module fun
implicit none
contains

function tg0(x)
implicit none
real(mp) tg0, tg, tg2, x
tg=tan(x)
tg2=tg*tg
tg0=sqrt(tg2+1.23_mp/cos(x))-tg
return
end function


function tg1(x)
implicit none
real(mp) sin2, tg1, x
tg1=1.23_mp/((sqrt(sin(x)*sin(x)+1.23_mp*cos(x))+sin(x)))
return
end function

end module fun
