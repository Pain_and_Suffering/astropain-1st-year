function trap(f,a,b,n)
implicit none
integer n, i
real a, b, trap, f
real h, x, s
h=(b-a)/n
s=(f(a)+f(b))/2.0
do i=2,n
    x=a+(i-1)*h
    s=s+f(x)
enddo
trap=s*h
end