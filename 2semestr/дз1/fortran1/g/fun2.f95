recursive integer function fun2(a) result(w)
implicit none
integer a,fun4,k
if (a.eq.2) then
w=3
else
  if (mod(a,2).eq.0) then
  a=a+1
  else
  a=a+2
  endif
    k=3
    if (fun4(a,k).eq.0) then
    w=a
    else
    w=fun2(a)
    endif
endif
end function

recursive integer function fun4(a,k) result(w)
implicit none
integer a,k
if (a.eq.2) then
w=0
else
  if (mod(a,2).eq.0) then
  w=1
  else
  w=0
    if ((mod(a,k).eq.0).and.(k.le.(a/k))) then
    w=1
    else
      if (k.le.(a/k)) then
      w=fun4(a,k+2)
      endif
    endif
  endif
endif
end function
