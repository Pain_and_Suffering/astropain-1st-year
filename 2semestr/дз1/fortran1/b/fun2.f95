recursive integer function fun2(a,b) result(w)
implicit none
integer a,b

if ((a-b).lt.b) then
w=a-b
else
w=fun2(a-b,b)
endif

end function
