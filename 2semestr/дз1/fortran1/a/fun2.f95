recursive integer function fun2(a,b) result(w)
implicit none
integer a,b

if (a.lt.b) then
w=0
else
if (a.eq.b) then
w=1
else
w=1+fun2(a-b,b)
endif
endif

end function
