function fun1(a)
implicit none
real a,fun1,eps,k

k=10
eps=1
fun1=0

do while ((a.gt.0._4).and.(eps.ge.0.000001))
 a=a*2
 fun1=fun1+int(a)/k
 k=k*10
 a=a-int(a)
 eps=eps/10
enddo

end function
