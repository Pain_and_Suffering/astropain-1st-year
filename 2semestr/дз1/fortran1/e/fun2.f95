recursive real function fun2(a,eps,k) result(w)
implicit none
real a,eps,k

if ((a.eq.0._4).or.(eps.le.0.000001)) then
 w=0
else
 eps=eps/10
 k=k*10
 w=int(a*2)/k+fun2(a*2-int(a*2),eps,k)
endif

end function
