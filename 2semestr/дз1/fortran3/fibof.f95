real*8 function fibof(n)
implicit none
integer n
real*8, parameter :: c5=sqrt(5d0)
real*8, parameter :: x=(1d0+c5)*0.5d0;
real*8, parameter :: y=(1d0-c5)*0.5d0;
fibof=(x**n-y**n)/c5
end
